import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        tiles: [],
    },
    getters: {
        results(state) {
            return state.tiles
        },
        getById: state => id => {
            console.log(state.tiles.filter(tile => tile.id === id))
            return state.tiles.filter(tile => tile.id === id);
        }
    },
    mutations: {
        set(state,{type,items}) {
            state[type] = items
        }
    },
    actions: {
        getTiles({commit}) {
            axios.get('api/tiles')
            .then( response => {
                const tiles  = response.data.tiles;
                commit('set', {type : 'tiles', items: tiles})
            }).catch(error => {
              console.log(error)
            })
        },
    }
})

export default store
