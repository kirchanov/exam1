import Vue from 'vue';

import App from './App.vue';

import router from './router';

import store from './store/index.js'


const app = new Vue({
    router,
    store:store,
    render(h) {
        return h(App);
    },
});

app.$mount(document.querySelector('#app'));
