import Vue from 'vue'
import Router from 'vue-router'
import Main from '../components/main.vue'
import Detail from '../components/detail.vue'
import Page404 from '../components/page404.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '*',
      name: '404',
      component: Page404
    }
  ]
})
